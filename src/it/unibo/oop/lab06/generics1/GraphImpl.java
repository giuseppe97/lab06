package it.unibo.oop.lab06.generics1;
import java.util.*;


public class GraphImpl<N> implements Graph<N> {
	
	private final Set<N> treeset = new TreeSet<>();
	private final Set<N> valueset = new TreeSet<>();
	private final Map<N,N> treemap = new TreeMap<>();
	private final List<N> connections = new ArrayList<>();
	
	
	public void addNode(N node) {
		this.treeset.add(node);
	}

	public void addEdge(N source, N target) {
		this.treemap.put(source, target);
	}

	public Set<N> nodeSet() {
		return this.treeset;
	}

	public Set<N> linkedNodes(N node) {
		for(N var:this.treemap.keySet() ) {
			if(var.equals(node)) {
				this.valueset.add(treemap.get(var));
			}
		}
		return this.valueset;
	}

	public List<N> getPath(N source, N target) {
		for(N var:this.treemap.keySet()) {
			if(var.equals(source)) {
				this.connections.add(source);
			}
		}
		for(N var:this.treemap.values()) {
			if(var.equals(target)) {
				this.connections.add(target);
			}
		}
		return this.connections;
	}
	
}














