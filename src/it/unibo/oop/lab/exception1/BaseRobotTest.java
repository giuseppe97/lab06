package it.unibo.oop.lab.exception1;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Testing class for PositionOutOfBound.
 * 
 */
public final class BaseRobotTest {

    @Test
    public void testRobotMovementBase() {
        
        final Robot r1 = new Robot("SimpleRobot", 100);
        // checking if robot is in position x=0; y=0
        assertEquals("[CHECKING ROBOT INIT POS X]", 0, r1.getEnvironment().getCurrPosX());
        assertEquals("[CHECKING ROBOT INIT POS Y]", 0, r1.getEnvironment().getCurrPosY());
        
        try {
        	while(true) {
        		r1.moveUp();
        	}
        } catch(Exception e) {
        	throw e;
        }
    }
	
    /**
     * Simple test for testing robot battery.
     * 
     */
    @Test
    public void testRobotBatteryBase() {
        final Robot r2 = new Robot("SimpleRobot2", 20);
        
        try {
        	while(true) {
        		r2.moveUp();
        		r2.moveDown();
        	}
        } catch(Exception e) {
        	throw e;
        }
        
    }
}
