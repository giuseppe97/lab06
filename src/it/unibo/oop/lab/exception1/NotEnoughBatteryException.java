package it.unibo.oop.lab.exception1;

public class NotEnoughBatteryException extends IllegalStateException {
	
	private static final long serialVersionUID = 1L;
	private final double batteryLevel;
	private final double requiredBattery;

	public NotEnoughBatteryException(final double level, final double required) {
		super();
		this.batteryLevel = level;
		this.requiredBattery = required;
	}

	public String getMessage() {
		return ( "you need " + this.requiredBattery + "but you have " + this.batteryLevel ) ;
	}
	
}
