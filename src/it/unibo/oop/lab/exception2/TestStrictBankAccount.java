package it.unibo.oop.lab.exception2;

import org.junit.Test;

/**
 * JUnit test to test
 * {@link StrictBankAccount}.
 * 
 */
public class TestStrictBankAccount {

    @Test
    public void testBankOperations() {
    	
    	final BankAccount luigi = new StrictBankAccount(1, 10000.0, 10);
    	final BankAccount mario = new StrictBankAccount(2, 10000.0, 10);
    	
    	luigi.deposit(5, 20.0);

    	luigi.withdraw(1, 2000000.0);

    	while(true) {
    		mario.depositFromATM(2, 40.0);
    	}
    	
    }
}
