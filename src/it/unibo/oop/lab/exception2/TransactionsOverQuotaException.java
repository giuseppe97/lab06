package it.unibo.oop.lab.exception2;

public class TransactionsOverQuotaException extends RuntimeException {

	private static final long serialVersionUID = 666;
	public String getMessage() {
		return "too many transactions ";
	}
}
