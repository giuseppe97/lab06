/**
 * 
 */
package it.unibo.oop.lab.collections2;
import java.util.*;


public class SocialNetworkUserImpl<U extends User> extends UserImpl implements SocialNetworkUser<U> {
	
	private final Map<U,String> followedUsers;
	private Set<U> groupUsers;
	private List<U> followed;
	
    public SocialNetworkUserImpl(final String name, final String surname, final String user, final int userAge) {
        super(name, surname, user, userAge);
        this.followedUsers = new HashMap<>();
    }
    
    public SocialNetworkUserImpl(final String name, final String surname, final String user) {
    	this(name, surname, user, -1);
    }

    public boolean addFollowedUser(final String circle, final U user) {
    	if(!this.followedUsers.containsKey(user)) {
    		this.followedUsers.put(user, circle);
    		return true;
    	}
    	return false;
    }

    public Collection<U> getFollowedUsersInGroup(final String groupName) {
    	this.groupUsers = new HashSet<>();
    	for(Map.Entry<U,String> tmp:this.followedUsers.entrySet()) {
    		if(tmp.getValue()==groupName) {
    			this.groupUsers.add(tmp.getKey());
    		}
    	}
        return this.groupUsers;
    }

    public List<U> getFollowedUsers() {
    	this.followed = new ArrayList<>();
    	this.followed.addAll(this.followedUsers.keySet());
    	return this.followed;
    }
    
}










