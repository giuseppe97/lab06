package it.unibo.oop.lab.collections1;
import java.util.*;


public final class UseCollection {
	
	private static final int ELEMS = 100000;
    private static final int TO_MS = 1000000;
    private static final int STOP = 2000;
    private static final int READS = 1000;

    private UseCollection() {
    }

    public static void main(final String... s) {
    	
    	final Collection<Integer> arraylist = new ArrayList<Integer>();
    	final Collection<Integer> linkedlist = new LinkedList<Integer>();
    	int finalint = 0;
    	int startint;
    	int tmp;
    	
    	for(int i=1000 ; i < STOP ; i++) {
    		arraylist.add(i);
    	}
    	
    	linkedlist.addAll(arraylist);
    	
    	final Iterator<Integer> itr1 = arraylist.iterator();
    	while(itr1.hasNext()) {
    		finalint = itr1.next();
    	}
    	final Iterator<Integer> itr2 = arraylist.iterator();
    	if(itr2.hasNext()) {
    		startint = itr2.next();
    		tmp = startint;
    		startint = finalint;
    		finalint = tmp;
    	}
    	
    	for( Integer var:arraylist ) {
    		System.out.println(var.toString());
    	}
    	
    	arraylist.clear();
    	linkedlist.clear();
    	long time = System.nanoTime();
    	for (int i = 1; i <= ELEMS; i++) {
    		arraylist.add(i);
    	}
    	time = System.nanoTime() - time;
    	System.out.println("Converting " + ELEMS + " int to String and inserting them in a Set took " + time + "ns (" + time / TO_MS + "ms)");
    	time = System.nanoTime();
    	for (int i = 1; i <= ELEMS; i++) {
    		linkedlist.add(i);
    	}
    	time = System.nanoTime() - time;
    	System.out.println("Converting " + ELEMS + " int to String and inserting them in a Set took " + time + "ns (" + time / TO_MS + "ms)");
    	
    	time = System.nanoTime();
    	for(int j=0; j<READS ; j++) {
    		Iterator<Integer> itr = arraylist.iterator();
    		for (int i=1; i<(ELEMS/2); i++) {
    			itr.next();
    		}
    		System.out.println(itr.next().toString());
    	}
    	time = System.nanoTime() - time;
    	System.out.println("picking " + ELEMS + " int to String and inserting them in a Set took " + time + "ns (" + time / TO_MS + "ms)");
    	time = System.nanoTime();
    	for(int j=0; j<READS ; j++) {
    		final Iterator<Integer> itrt = linkedlist.iterator();
    		for (int i=1; i<(ELEMS/2); i++) {
    			itrt.next();
    		}
    		System.out.println(itrt.next().toString());
    	}
    	time = System.nanoTime() - time;
    	System.out.println("picking " + ELEMS + " int to String and inserting them in a Set took " + time + "ns (" + time / TO_MS + "ms)");
    	
    	final Map<String, Integer> treemap = new TreeMap<>();
    	Integer sum = 0;
    	treemap.put("Africa", 1110635000);
    	treemap.put("Americas", 972005000);
    	treemap.put("Antartica", 1110635000);
    	treemap.put("Asia", 1110635000);
    	treemap.put("Europe", 1110635000);
    	treemap.put("Oceania", 1110635000);
    	for( Integer var:treemap.values() ) {
    		sum = sum + var ;
    	}
    	System.out.println(sum.toString());
        
    }
}
